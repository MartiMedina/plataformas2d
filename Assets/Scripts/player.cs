﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class player : MonoBehaviour
{

    private Rigidbody2D rb2d = null;
    private float move = 2.0f;
    private bool jump;
    public float maxS = 11f;
    public float fuerzaSalto = 11f;
    // Use this for initialization
    void Awake()
    {
        rb2d = GetComponent<Rigidbody2D>();
    }
    // Update is called once per frame
    void FixedUpdate()
    {
        
        rb2d.velocity = new Vector2(move * maxS, rb2d.velocity.y);
    }

    private void Update()
    {
        move = Input.GetAxis("Horizontal");
        jump = Input.GetKeyDown(KeyCode.Space);

        if (jump)
        {
            jump = false;
            rb2d.AddForce(Vector2.up * fuerzaSalto, ForceMode2D.Impulse);
        }
    }
}
